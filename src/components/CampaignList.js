import React, {PropTypes} from 'react'
import List from 'material-ui/List'
import Campaign from './Campaign'

const drawCampaign = (campaigns, onChangeCampaign) => {
  return campaigns.map( (campaign) => (
    <Campaign
        {...campaign}
        onSelectCampaign={onChangeCampaign}
        key={campaign._id}
    />
  ))
}

const CampaignList = ( {campaigns, onChangeCampaign} ) => (
  <List>
    {drawCampaign(campaigns, onChangeCampaign)}
  </List>
)

CampaignList.propTypes = {
  campaigns : PropTypes.array
}

export default CampaignList
