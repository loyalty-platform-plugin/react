import React, { PropTypes } from 'react'
import ListItem from 'material-ui/List/ListItem'

const Campaign = ({_id, name, partner, url, onSelectCampaign}) => (
  <ListItem
    primaryText={name}
    secondaryText={partner}
    onClick={(event) => onSelectCampaign(url) }
  />
)

Campaign.propTypes = {
  _id : PropTypes.string.isRequired,
  name : PropTypes.string.isRequired,
  partner : PropTypes.string.isRequired,
  url : PropTypes.string.isRequired,
  onSelectCampaign : PropTypes.func.isRequired,
}

export default Campaign
