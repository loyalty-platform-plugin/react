import {API_ENTRY_POINT} from '../config/api'
export const REQUEST_CAMPAIGN_LIST = 'REQUEST_CAMPAIGN_LIST';
export const RESPONSE_CAMPAIGN_LIST = 'RESPONSE_CAMPAIGN_LIST';

function requestCampaignList() {
	return {
		type : REQUEST_CAMPAIGN_LIST
	}
}

function responseCampaignList(json) {
	return {
		type : RESPONSE_CAMPAIGN_LIST,
		data : json
	}
}

export function fetchCampaignList() {
	return (dispatch => {
		dispatch(requestCampaignList())
		return fetch(API_ENTRY_POINT)
			.then(response => response.json())
			.then(json => dispatch(responseCampaignList(json)))
			.catch(error => console.log(error))
	})
}
