import { connect } from 'react-redux'
import CampaignList from '../components/CampaignList'

const mapStateToProps = (state) => state

const mapDispatchToProps = (dispatch) => {
	return {
		onChangeCampaign : (url) => {
      window.open(url, '_blank')
		}
	}
}

const CampaignListContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(CampaignList)

export default CampaignListContainer
