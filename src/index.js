import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import App from './App'
import pluginApp from './reducers'
import { fetchCampaignList } from './actions/Campaign'

let store = createStore(
	pluginApp,
	{},
	applyMiddleware(
		thunkMiddleware
	)
)

store.dispatch(fetchCampaignList())

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
  document.getElementById('loyalty-platform-plugin')
)
