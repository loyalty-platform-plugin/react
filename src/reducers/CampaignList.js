import * as Action from '../actions/Campaign'

const campaignList = ( state = [], action) => {
    switch (action.type) {
      case Action.REQUEST_CAMPAIGN_LIST:
        return state // TODO: udate loading  status
      case Action.RESPONSE_CAMPAIGN_LIST:
        return action.data
      default:
        return state
    }
}

export default campaignList
