import { combineReducers } from 'redux'
import CampaignList from './CampaignList'

const pluginApp = combineReducers({
	campaigns : CampaignList,
})

export default pluginApp
