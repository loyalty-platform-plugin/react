import React from 'react'
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import injectTapEventPlugin from 'react-tap-event-plugin'
import CampaignList from './containers/CampaignList'

injectTapEventPlugin()

export default class AppView extends React.Component {

	getChildContext = () => {
		return { muiTheme: getMuiTheme(baseTheme) }
	}

	render = () => (
		<CampaignList />
	)
}

AppView.childContextTypes = {
	muiTheme: React.PropTypes.object.isRequired,
}
